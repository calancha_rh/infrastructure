#!/usr/bin/bash

set -euo pipefail

if rpm -q kernel-core | tail -n 1 | grep -F "$(uname -r)"; then
    exit 0
fi

IFS=' ' read -ra services <<< "${SERVICES_TO_STOP:-}"
for service in "${services[@]}"; do
    while ! systemctl stop "${service}"; do
        echo "Failed to stop ${service}, retrying"
    done
done

exit 1
