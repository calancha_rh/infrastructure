#!/usr/bin/bash

set -euo pipefail

dnf offline-upgrade download --assumeyes

IFS=' ' read -ra services <<< "${SERVICES_TO_STOP:-}"
for service in "${services[@]}"; do
    while ! systemctl stop "${service}"; do
        echo "Failed to stop ${service}, retrying"
    done
done

# spin up the services again if the upgrade cannot be scheduled
if ! dnf offline-upgrade reboot; then
    for service in "${services[@]}"; do
        systemctl start "${service}" || true
    done
fi
